package models

type InputMoney struct {
	Bank1000    int
	Bank500     int
	Bank100     int
	Bank20      int
	Coin10      int
	Coin5       int
	Coin2       int
	Coin1       int
	CoinPoint50 int
	CoinPoint25 int
}

func (s *InputMoney) TotalOfBank() int {
	return s.Bank1000 + s.Bank500 + s.Bank100 + s.Bank20
}

func (s *InputMoney) TotalOfCoin() int {
	return s.Coin10 + s.Coin5 + s.Coin2 + s.Coin1 + s.CoinPoint50 + s.CoinPoint25
}

func (s *InputMoney) Value() float64 {

	var value float64

	value += float64(s.Bank1000 * 1000)
	value += float64(s.Bank500 * 500)
	value += float64(s.Bank100 * 100)
	value += float64(s.Bank20 * 20)
	value += float64(s.Coin10 * 10)
	value += float64(s.Coin5 * 5)
	value += float64(s.Coin2 * 2)
	value += float64(s.Coin1 * 1)
	value += float64(s.CoinPoint50) * 0.5
	value += float64(s.CoinPoint25) * 0.25

	return value
}
