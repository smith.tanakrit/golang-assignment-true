package main

import (
	"calculator/models"
	"fmt"
	"log"
	"strings"
)

const (
	MaximumOfTotal = 1200
	MaximumOfBank  = 1000
	MaximumOfCoin  = 500
)

func main() {
	input1 := models.InputMoney{
		Bank1000: 2,
		Coin10:   3,
	}

	strResult, err := ConvertToHumanTextTHB("th", input1)

	if err != nil {
		log.Fatal(err)
		return
	}

	log.Print(strResult)
}

func ConvertToHumanTextTHB(lang string, money models.InputMoney) (string, error) {

	/// validate check lang before
	lang = strings.ToLower(lang)

	if lang != "th" && lang != "en" {
		return "", fmt.Errorf("ConvertToHumanTextTHB : invalid lang")
	}

	/// bank check condition
	totalBank := money.TotalOfBank()

	if totalBank > MaximumOfBank {
		return "", fmt.Errorf("ConvertToHumanTextTHB : total banks is more than %v", MaximumOfBank)
	}

	/// coin check condition
	totalCoin := money.TotalOfCoin()

	if totalCoin > MaximumOfCoin {
		return "", fmt.Errorf("ConvertToHumanTextTHB : total coins is more than %v", MaximumOfCoin)
	}

	/// check all
	if totalCoin+totalBank > MaximumOfTotal {
		return "", fmt.Errorf("ConvertToHumanTextTHB : total coins/banks is more than %v", MaximumOfTotal)
	}

	humanText := ""

	if money.Bank1000 > 0 {
		humanText += getLocalNumber(lang, money.Bank1000) + getLocalUnit(lang, 1000)
	}

	if money.Bank500 > 0 {
		humanText += getLocalNumber(lang, money.Bank500) + getLocalUnit(lang, 500)
	}

	if money.Bank100 > 0 {
		humanText += getLocalNumber(lang, money.Bank100) + getLocalUnit(lang, 100)
	}

	if money.Bank20 > 0 {
		humanText += getLocalNumber(lang, money.Bank20) + getLocalUnit(lang, 20)
	}

	if money.Coin10 > 0 {
		humanText += getLocalNumber(lang, money.Coin10) + getLocalUnit(lang, 10)
	}

	if money.Coin5 > 0 {
		humanText += getLocalNumber(lang, money.Coin5) + getLocalUnit(lang, 5)
	}

	if money.Coin2 > 0 {
		humanText += getLocalNumber(lang, money.Coin2) + getLocalUnit(lang, 2)
	}

	if money.Coin1 > 0 {
		humanText += getLocalNumber(lang, money.Coin1) + getLocalUnit(lang, 1)
	}

	log.Print("total value is ", money.Value())

	return humanText, nil

}

func getLocalNumber(lang string, number int) string {
	switch lang {
	case "th":
		switch number {
		case 1:
			return "หนึ่ง"
		case 2:
			return "สอง"
		case 3:
			return "สาม"
		case 4:
			return "สี่"
		case 5:
			return "ห้า"
		case 6:
			return "หก"
		case 7:
			return "เจ็ด"
		case 8:
			return "แปด"
		case 9:
			return "เก้า"
		case 0:
			return ""
		}
	case "en":
		switch number {
		case 1:
			return "One"
		case 2:
			return "Two"
		case 3:
			return "Three"
		case 4:
			return "Four"
		case 5:
			return "Five"
		case 6:
			return "Six"
		case 7:
			return "Seven"
		case 8:
			return "Eight"
		case 9:
			return "Nine"
		case 0:
			return ""
		}
	}

	return ""
}

func getLocalUnit(lang string, number int) string {
	switch lang {
	case "th":
		switch number {
		case 1000:
			return "พัน"
		case 100:
			return "ร้อย"
		case 10:
			return "สิบ"
		}
	case "en":
		switch number {
		case 1000:
			return "thousand"
		case 100:
			return "hundred"
		case 10:
			return "ty"
		}
	}

	return ""
}
