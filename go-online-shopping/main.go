package main

import (
	"go-online-shopping/configs"
	"go-online-shopping/routes"
	"go-online-shopping/services/account"
	"go-online-shopping/services/cart"
	"go-online-shopping/services/order"
	"go-online-shopping/services/product"
	"os"
)

func main() {
	configs.LoadEnv()
	configs.SetUpDB()
	defer configs.CloseDB()

	migration()

	router := routes.Router()

	router.Run(":" + os.Getenv("ROUTER_PORT"))
}

func migration() {
	configs.DB.AutoMigrate(
		&account.Account{},
		&product.Product{},
		&cart.Cart{},
		&order.Order{},
	)
}
