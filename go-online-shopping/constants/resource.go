package constants

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func SuccessResponse(context *gin.Context, data interface{}) {
	context.JSON(http.StatusOK, gin.H{
		"data":    data,
		"status": map[string]interface{}{
			"code": 200,
			"message": "OK",
		},
	})
}

func NotFoundResponse(context *gin.Context,  message string, data interface{}) {
	if message == "" {
		message = "NOT_FOUND"
	}

	context.JSON(http.StatusNotFound, gin.H{
		"data":    data,
		"status": map[string]interface{}{
			"code": 404,
			"message": message,
		},
	})
}

func ErrorResponse(context *gin.Context,  message string, data interface{})  {
	if message == "" {
		message = "SOMETHING_WENT_WRONG"
	}

	context.JSON(http.StatusBadRequest, gin.H{
		"data":    data,
		"status": map[string]interface{}{
			"code": 400,
			"message": message,
		},
	})
}
