package account

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"go-online-shopping/Http"
	"go-online-shopping/constants"
	"golang.org/x/crypto/bcrypt"
	"strconv"
)

func GetAllAccountController(ctx *gin.Context) {
	var accounts []Account
	var err error

	err = GetAllAccount(&accounts)

	if err != nil {
		constants.NotFoundResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, accounts)
	}
}

func GetAccountByIdController(ctx *gin.Context) {
	var account Account
	var err error

	id := ctx.Params.ByName("id")
	accountId, _ := strconv.Atoi(id)

	err = GetAccountById(accountId, &account)

	if err != nil {
		constants.NotFoundResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, account)
	}
}

func CreateAccountController(ctx *gin.Context) {
	var account Account
	var err error

	err = ctx.BindJSON(&account)

	if err != nil {
		constants.ErrorResponse(ctx, "CREATE_ACCOUNT_VALIDATE_ERROR", []int{})
		return
	}

	account.Password, _ = hashPassword(account.Password)
	err = CreateAccount(&account)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, account)
	}
}

func UpdateAccountController(ctx *gin.Context) {
	var account Account
	var updateAccountInput map[string]interface{}
	var err error

	id := ctx.Params.ByName("id")
	accountId, _ := strconv.Atoi(id)

	if err = GetAccountById(accountId, &account); err != nil {
		constants.NotFoundResponse(ctx, "ACCOUNT_NOT_FOUND", []int{})
		return
	}

	updateAccountInput = http.GetRequestBody(ctx, account.getFillAble())

	if updatePassword, exist := updateAccountInput["password"]; exist {
		updateAccountInput["password"], _ = hashPassword(fmt.Sprint(updatePassword))
	}

	err = UpdateAccount(&account, updateAccountInput)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, account)
	}
}

func DeleteAccountController(ctx *gin.Context) {
	var account Account
	var err error

	id := ctx.Params.ByName("id")
	accountId, _ := strconv.Atoi(id)

	if err = GetAccountById(accountId, &account); err != nil {
		constants.NotFoundResponse(ctx, "ACCOUNT_NOT_FOUND", []int{})
		return
	}

	err = DeleteAccount(&account)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, []int{})
	}
}

type LogInRequest struct {
	AccountId uint   `json:"account_id"`
	Password  string `json:"password"`
}

func LogInController(ctx *gin.Context) {
	var loginRequest LogInRequest
	var account Account;

	err := ctx.BindJSON(&loginRequest)

	if err != nil {
		constants.ErrorResponse(ctx, "LOGIN_VALIDATE_ERROR", []int{})
		return
	}

	if err = GetAccountById(int(loginRequest.AccountId), &account); err != nil {
		constants.NotFoundResponse(ctx, "ACCOUNT_NOT_FOUND", []int{})
		return
	}

	if checkPasswordHash(loginRequest.Password, account.Password) != true {
		constants.ErrorResponse(ctx, "LOGIN_FAIL_CREDENTIAL_MISMATCH", []int{})
		return
	}

	constants.SuccessResponse(ctx, account)
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
