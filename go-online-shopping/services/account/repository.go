package account

import (
	"go-online-shopping/configs"
)

func GetAllAccount(accounts *[]Account) (err error) {
	query := configs.DB

	err = query.Find(accounts).Error

	if err != nil {
		return err
	}

	return nil
}

func GetAccountById(id int, account *Account) (err error) {
	if err = configs.DB.Where("id"+" = ?", id).First(account).Error; err != nil {
		return err
	}

	return nil
}

func CreateAccount(account *Account) (err error) {
	result := configs.DB.Create(&account)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}

func UpdateAccount(account *Account, updateAccountInput map[string]interface{}) (err error) {
	result := configs.DB.Model(&account).Updates(updateAccountInput)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}

func DeleteAccount(account *Account) (err error) {
	result := configs.DB.Delete(&account)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}
