package account

type Account struct {
	ID           uint   `gorm:"primaryKey" json:"id,omitempty"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Sex          string `json:"sex"`
	Age          int    `json:"age"`
	MobileNumber string `json:"mobile_number"`
	Password     string `json:"password"`
	Address      string `json:"address"`
}

func (account Account) getFillAble() []string {
	return []string{
		"first_name",
		"last_name",
		"sex",
		"age",
		"mobile_number",
		"password",
		"address",
	}
}
