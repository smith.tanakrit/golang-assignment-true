package cart

import (
	"github.com/gin-gonic/gin"
	"go-online-shopping/Http"
	"go-online-shopping/constants"
	"go-online-shopping/services/order"
	"go-online-shopping/services/product"
	"strconv"
)

func GetAllCartController(ctx *gin.Context) {
	var carts []Cart
	var err error

	err = GetAllCart(&carts)

	if err != nil {
		constants.NotFoundResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, carts)
	}
}

func GetCartByIdController(ctx *gin.Context) {
	var cart Cart
	var err error

	id := ctx.Params.ByName("id")
	cartId, _ := strconv.Atoi(id)

	err = GetCartById(cartId, &cart)

	if err != nil {
		constants.NotFoundResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, cart)
	}
}

func CreateCartController(ctx *gin.Context) {
	var cart Cart
	var err error

	err = ctx.BindJSON(&cart)

	if err != nil {
		constants.ErrorResponse(ctx, "CREATE_CART_VALIDATE_ERROR", []int{})
		return
	}

	cart.Status = "waiting"
	err = CreateCart(&cart)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, cart)
	}
}

func UpdateCartController(ctx *gin.Context) {
	var cart Cart
	var updateCartInput map[string]interface{}
	var err error

	id := ctx.Params.ByName("id")
	cartId, _ := strconv.Atoi(id)

	if err = GetCartById(cartId, &cart); err != nil {
		constants.NotFoundResponse(ctx, "CART_NOT_FOUND", []int{})
		return
	}
	updateCartInput = http.GetRequestBody(ctx, cart.getFillAble())
	err = UpdateCart(&cart, updateCartInput)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, cart)
	}
}

func DeleteCartController(ctx *gin.Context) {
	var cart Cart
	var err error

	id := ctx.Params.ByName("id")
	cartId, _ := strconv.Atoi(id)

	if err = GetCartById(cartId, &cart); err != nil {
		constants.NotFoundResponse(ctx, "CART_NOT_FOUND", []int{})
		return
	}

	err = DeleteCart(&cart)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, []int{})
	}
}

func CheckoutController(ctx *gin.Context)  {
	var cart Cart
	var err error

	id := ctx.Params.ByName("id")
	cartId, _ := strconv.Atoi(id)

	if err = GetCartById(cartId, &cart); err != nil {
		constants.NotFoundResponse(ctx, "CART_NOT_FOUND", []int{})
		return
	}

	var totalPrice float32

	for _, item := range cart.GetItem() {
		var productData product.Product

		if err := product.GetProductById(int(item.ProductId), &productData); err != nil {
			continue
		}

		totalPrice += productData.Price * float32(item.Count)
	}

	// Update Cart Status
	err = UpdateCart(&cart, map[string]interface{}{"status": "checkout"})

	// CreateOrder
	orderData := order.Order{
		CartId: cart.ID,
		AccountId: cart.AccountId,
		TotalPrice: totalPrice,
		Status: "waiting",
	}
	err = order.CreateOrder(&orderData)

	response := map[string]interface{}{
		"Cart": cart,
		"TotalPrice": totalPrice,
	}

	if err != nil {
		constants.ErrorResponse(ctx, "CHECKOUT_ERROR", []int{})
	} else {
		constants.SuccessResponse(ctx, response)
	}
}
