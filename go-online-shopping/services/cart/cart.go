package cart

import (
	"encoding/json"
	"gorm.io/datatypes"
)

type Cart struct {
	ID        uint           `gorm:"primaryKey" json:"id,omitempty"`
	AccountId uint           `gorm:"index" json:"account_id,omitempty"`
	Item      datatypes.JSON `json:"item"`
	Status    string         `json:"status"`
}

type Item struct {
	ProductId uint `json:"product_id"`
	Count     int  `json:"count"`
}

func (cart Cart) getFillAble() []string {
	return []string{
		"account_id",
		"item",
		"status",
	}
}

func (cart *Cart) GetItem() []Item {
	var items []Item
	json.Unmarshal(cart.Item, &items)

	return items
}
