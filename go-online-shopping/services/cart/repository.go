package cart

import (
	"go-online-shopping/configs"
)

func GetAllCart(carts *[]Cart) (err error) {
	query := configs.DB

	err = query.Find(carts).Error

	if err != nil {
		return err
	}

	return nil
}

func GetCartById(id int, cart *Cart) (err error) {
	if err = configs.DB.Where("id"+" = ?", id).First(cart).Error; err != nil {
		return err
	}

	return nil
}

func CreateCart(cart *Cart) (err error) {
	result := configs.DB.Create(&cart)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}

func UpdateCart(cart *Cart, updateCartInput map[string]interface{}) (err error) {
	result := configs.DB.Model(&cart).Updates(updateCartInput)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}

func DeleteCart(cart *Cart) (err error) {
	result := configs.DB.Delete(&cart)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}
