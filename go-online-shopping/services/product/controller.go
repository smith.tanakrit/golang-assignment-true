package product

import (
	"github.com/gin-gonic/gin"
	"go-online-shopping/Http"
	"go-online-shopping/constants"
	"strconv"
)

func GetAllProductController(ctx *gin.Context) {
	var products []Product
	var err error

	err = GetAllProduct(&products)

	if err != nil {
		constants.NotFoundResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, products)
	}
}

func GetProductByIdController(ctx *gin.Context) {
	var product Product
	var err error

	id := ctx.Params.ByName("id")
	productId, _ := strconv.Atoi(id)

	err = GetProductById(productId, &product)

	if err != nil {
		constants.NotFoundResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, product)
	}
}

func CreateProductController(ctx *gin.Context) {
	var product Product
	var err error

	err = ctx.BindJSON(&product)

	if err != nil {
		constants.ErrorResponse(ctx, "CREATE_PRODUCT_VALIDATE_ERROR", []int{})
		return
	}

	err = CreateProduct(&product)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, product)
	}
}

func UpdateProductController(ctx *gin.Context) {
	var product Product
	var updateProductInput map[string]interface{}
	var err error

	id := ctx.Params.ByName("id")
	productId, _ := strconv.Atoi(id)

	if err = GetProductById(productId, &product); err != nil {
		constants.NotFoundResponse(ctx, "PRODUCT_NOT_FOUND", []int{})
		return
	}
	updateProductInput = http.GetRequestBody(ctx, product.getFillAble())
	err = UpdateProduct(&product, updateProductInput)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, product)
	}
}

func DeleteProductController(ctx *gin.Context) {
	var product Product
	var err error

	id := ctx.Params.ByName("id")
	productId, _ := strconv.Atoi(id)

	if err = GetProductById(productId, &product); err != nil {
		constants.NotFoundResponse(ctx, "PRODUCT_NOT_FOUND", []int{})
		return
	}

	err = DeleteProduct(&product)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, []int{})
	}
}
