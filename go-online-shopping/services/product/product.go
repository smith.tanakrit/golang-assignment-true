package product

type Product struct {
	ID       uint    `gorm:"primaryKey" json:"id,omitempty"`
	Name     string  `json:"name"`
	Category string  `json:"category"`
	Count    int     `json:"count"`
	Price    float32 `json:"price"`
}

func (product Product) getFillAble() []string {
	return []string{
		"name",
		"category",
		"count",
		"price",
	}
}
