package product

import (
	"go-online-shopping/configs"
)

func GetAllProduct(products *[]Product) (err error) {
	query := configs.DB

	err = query.Find(products).Error

	if err != nil {
		return err
	}

	return nil
}

func GetProductById(id int, product *Product) (err error) {
	if err = configs.DB.Where("id"+" = ?", id).First(product).Error; err != nil {
		return err
	}

	return nil
}

func CreateProduct(product *Product) (err error) {
	result := configs.DB.Create(&product)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}

func UpdateProduct(product *Product, updateProductInput map[string]interface{}) (err error) {
	result := configs.DB.Model(&product).Updates(updateProductInput)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}

func DeleteProduct(product *Product) (err error) {
	result := configs.DB.Delete(&product)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}
