package order

import (
	"go-online-shopping/configs"
)

func GetAllOrder(orders *[]Order) (err error) {
	query := configs.DB

	err = query.Find(orders).Error

	if err != nil {
		return err
	}

	return nil
}

func GetOrderById(id int, order *Order) (err error) {
	if err = configs.DB.Where("id"+" = ?", id).First(order).Error; err != nil {
		return err
	}

	return nil
}

func CreateOrder(order *Order) (err error) {
	result := configs.DB.Create(&order)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}

func UpdateOrder(order *Order, updateOrderInput map[string]interface{}) (err error) {
	result := configs.DB.Model(&order).Updates(updateOrderInput)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}

func DeleteOrder(order *Order) (err error) {
	result := configs.DB.Delete(&order)

	if resErr := result.Error; resErr != nil {
		err = result.Error
	}

	return err
}
