package order

type Order struct {
	ID         uint    `gorm:"primaryKey" json:"id,omitempty"`
	CartId     uint    `gorm:"index" json:"cart_id"`
	AccountId  uint    `gorm:"index" json:"account_id"`
	TotalPrice float32 `json:"total_price"`
	Status     string  `json:"status"`
}

func (order Order) getFillAble() []string {
	return []string{
		"cart_id",
		"account_id",
		"total_price",
		"status",
	}
}
