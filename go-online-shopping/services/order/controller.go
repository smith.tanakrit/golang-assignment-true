package order

import (
	"github.com/gin-gonic/gin"
	"go-online-shopping/Http"
	"go-online-shopping/constants"
	"strconv"
)

func GetAllOrderController(ctx *gin.Context) {
	var orders []Order
	var err error

	err = GetAllOrder(&orders)

	if err != nil {
		constants.NotFoundResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, orders)
	}
}

func GetOrderByIdController(ctx *gin.Context) {
	var order Order
	var err error

	id := ctx.Params.ByName("id")
	orderId, _ := strconv.Atoi(id)

	err = GetOrderById(orderId, &order)

	if err != nil {
		constants.NotFoundResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, order)
	}
}

func CreateOrderController(ctx *gin.Context) {
	var order Order
	var err error

	err = ctx.BindJSON(&order)

	if err != nil {
		constants.ErrorResponse(ctx, "CREATE_ORDER_VALIDATE_ERROR", []int{})
		return
	}

	err = CreateOrder(&order)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, order)
	}
}

func UpdateOrderController(ctx *gin.Context) {
	var order Order
	var updateOrderInput map[string]interface{}
	var err error

	id := ctx.Params.ByName("id")
	orderId, _ := strconv.Atoi(id)

	if err = GetOrderById(orderId, &order); err != nil {
		constants.NotFoundResponse(ctx, "ORDER_NOT_FOUND", []int{})
		return
	}
	updateOrderInput = http.GetRequestBody(ctx, order.getFillAble())
	err = UpdateOrder(&order, updateOrderInput)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, order)
	}
}

func DeleteOrderController(ctx *gin.Context) {
	var order Order
	var err error

	id := ctx.Params.ByName("id")
	orderId, _ := strconv.Atoi(id)

	if err = GetOrderById(orderId, &order); err != nil {
		constants.NotFoundResponse(ctx, "ORDER_NOT_FOUND", []int{})
		return
	}

	err = DeleteOrder(&order)

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, []int{})
	}
}

func PayOrderController(ctx *gin.Context)  {
	var order Order
	var err error

	id := ctx.Params.ByName("id")
	orderId, _ := strconv.Atoi(id)

	if err = GetOrderById(orderId, &order); err != nil {
		constants.NotFoundResponse(ctx, "ORDER_NOT_FOUND", []int{})
		return
	}

	err = UpdateOrder(&order, map[string]interface{}{"status": "complete"})

	if err != nil {
		constants.ErrorResponse(ctx, "", []int{})
	} else {
		constants.SuccessResponse(ctx, order)
	}
}