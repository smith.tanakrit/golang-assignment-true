package configs

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"strconv"
)

var DB *gorm.DB = nil

type DBConfig struct {
	Host     string
	Port     int
	User     string
	DBName   string
	Password string
}

func buildDBConfig() *DBConfig {
	port, _ := strconv.Atoi(os.Getenv("DB_PORT"))

	dbConfig := DBConfig{
		Host:     os.Getenv("DB_HOST"),
		Port:     port,
		User:     os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
		DBName:   os.Getenv("DB_DATABASE"),
	}

	return &dbConfig
}

func dbURL(dbConfig *DBConfig) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local&multiStatements=true",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DBName,
	)
}

func SetUpDB() {
	if DB == nil {
		var err error
		gormConfig := gorm.Config{}

		if isDebugMode, _ := strconv.ParseBool(os.Getenv("DB_DEBUG_MODE")); isDebugMode == false {
			gormConfig.Logger = logger.Default.LogMode(logger.Silent)
		}

		DB, err = gorm.Open(
			mysql.Open(dbURL(buildDBConfig())),
			&gormConfig,
		)

		if err != nil {
			fmt.Println("Status:", err)
		}
	}
}

func CloseDB() {
	if DB != nil {
		sqlDB, err := DB.DB()

		if err != nil {
			log.Fatalln(err)
			return
		}

		sqlDB.Close()
	}
}
