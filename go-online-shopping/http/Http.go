package http

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func PostRequest(url string, body map[string]interface{}, headers map[string]string) string {
	defer func() {
		if err := recover(); err != nil {
			log.Println("Http Post Request Fail", err)
		}
	}()

	var err error = nil
	requestBody, jsonErr := json.Marshal(body)

	if jsonErr != nil {
		log.Println(err.Error())
	}

	client := http.Client{}
	request, reqErr := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))
	setHeader(request, headers)

	if reqErr != nil {
		log.Println(reqErr.Error())
	}

	response, resErr := client.Do(request)

	if resErr != nil {
		log.Println(resErr)
	}

	defer response.Body.Close()
	responseData, readResErr := ioutil.ReadAll(response.Body)

	if readResErr != nil {
		log.Println(readResErr.Error())
	}

	return string(responseData)
}

func setHeader(request *http.Request, headers map[string]string) {
	request.Header.Set("Content-Type", "application/json; charset=utf-8")

	for key, value := range headers {
		request.Header.Set(key, value)
	}
}

func CPMEndpoint() string {
	return strings.Trim(os.Getenv("CPM_API_ENDPOINT"), "/")
}

func GetRequestBody(c *gin.Context, expectData []string) map[string]interface{} {
	contentType := strings.ToLower(c.GetHeader("Content-Type"))
	body := map[string]interface{}{}

	if strings.Contains(contentType, "json") {
		allBody := map[string]interface{}{}
		c.BindJSON(&allBody)

		for _, get := range expectData {
			if data, exist := allBody[get]; exist {
				body[get] = data
			} else {
				body[get] = nil
			}
		}
	} else {
		for _, get := range expectData {
			if data, exist := c.GetPostForm(get); exist {
				body[get] = data
			} else {
				body[get] = nil
			}
		}
	}

	return body
}

func GetRequestBodyWithDefault(c *gin.Context, expectData map[string]interface{}) map[string]interface{} {
	contentType := strings.ToLower(c.GetHeader("Content-Type"))
	body := map[string]interface{}{}

	if strings.Contains(contentType, "json") {
		allBody := map[string]interface{}{}
		c.BindJSON(&allBody)

		for key, defaultVal := range expectData {
			if data, exist := allBody[key]; exist {
				body[key] = data
			} else {
				body[key] = defaultVal
			}
		}
	} else {
		for key, defaultVal := range expectData {
			if data, exist := c.GetPostForm(key); exist {
				body[key] = data
			} else {
				body[key] = defaultVal
			}
		}
	}

	return body
}
