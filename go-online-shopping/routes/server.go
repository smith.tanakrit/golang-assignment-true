package routes

import (
	"github.com/gin-gonic/gin"
	"go-online-shopping/services/account"
	"go-online-shopping/services/cart"
	"go-online-shopping/services/order"
	"go-online-shopping/services/product"
)

func Router() *gin.Engine {
	req := gin.Default()

	api := req.Group("api")
	{
		accounts := api.Group("accounts")
		{
			accounts.GET("/", account.GetAllAccountController)
			accounts.GET(":id", account.GetAccountByIdController)
			accounts.POST("/", account.CreateAccountController)
			accounts.PATCH(":id", account.UpdateAccountController)
			accounts.DELETE(":id", account.DeleteAccountController)

			accounts.POST("/login", account.LogInController)
		}

		products := api.Group("products")
		{
			products.GET("/", product.GetAllProductController)
			products.GET(":id", product.GetProductByIdController)
			products.POST("/", product.CreateProductController)
			products.PATCH(":id", product.UpdateProductController)
			products.DELETE(":id", product.DeleteProductController)
		}

		carts := api.Group("carts")
		{
			carts.GET("/", cart.GetAllCartController)
			carts.GET(":id", cart.GetCartByIdController)
			carts.POST("/", cart.CreateCartController)
			carts.PATCH(":id", cart.UpdateCartController)
			carts.DELETE(":id", cart.DeleteCartController)

			carts.POST("/checkout/:id", cart.CheckoutController)
		}

		orders := api.Group("orders")
		{
			orders.GET("/", order.GetAllOrderController)
			orders.GET(":id", order.GetOrderByIdController)
			orders.POST("/", order.CreateOrderController)
			orders.PATCH(":id", order.UpdateOrderController)
			orders.DELETE(":id", order.DeleteOrderController)

			orders.POST(":id", order.PayOrderController)
		}
	}

	return req
}
