module go-online-shopping

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/text v0.3.5 // indirect
	gorm.io/datatypes v1.0.1
	gorm.io/driver/mysql v1.1.0
	gorm.io/gorm v1.21.10
)
